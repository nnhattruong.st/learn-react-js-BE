package com.example.learnreactjsBE;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LearnReactJsBeApplication {

	public static void main(String[] args) {
		SpringApplication.run(LearnReactJsBeApplication.class, args);
	}

}
